package com.antra.raghu;
/**
 * Find the missing number in a given Array from number 1 to n 
 * @author Shettem Raghavendra
 *
 */
public class TestMissingElementInArray {

	public static void main(String[] args) {
		int[] arr = {1,2,3,4,5,8,9,7};
		int n = arr.length + 1;
		int sumTotal = (n * (n+1))/2;
		int sum = 0;
		for(int i : arr) {
			sum += i;
		}
		System.out.println(sumTotal-sum);
	}
}
