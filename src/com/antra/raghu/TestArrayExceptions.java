package com.antra.raghu;
/**
 * ArrayStoreException and ArrayIndexOutOfBoundsException
 * @author Shettem Raghavendra
 *
 */
public class TestArrayExceptions {

	public static void main(String[] args) {
		Object[] arr = new String[2];
		arr[0]=Integer.valueOf(0);
		System.out.println(arr);
		System.out.println(arr[2]);
	}
}
