package com.antra.raghu;

import java.util.Arrays;

/**
 * program to search a specific element in an Array
 * @author Shettem Raghavendra
 *
 */
public class TestFindGivenElementInArray {

	public static void main(String[] args) {
		int[] arr = {15,45,67,8,19,28,12};
		int element = 12;
		//inbuilt method (without sorting)
		/*int pos = Arrays.binarySearch(arr, 8);
		System.out.println(pos);*/
		Arrays.sort(arr);
		System.out.println(Arrays.toString(arr));
		int pos = myBinarySearch(arr,0,arr.length-1,element);
		System.out.println(pos);
	}

	private static int myBinarySearch(int[] arr, int left, int right, int element) {
		if(right > left) {
			int mid = (left + right) / 2;
			if(arr[mid] == element)
				return mid;
			else if(arr[mid] > element)
				return myBinarySearch(arr, left, mid-1, element);
			else 
				return myBinarySearch(arr, mid+1, right, element);
		}
		return -1;
	}
}
